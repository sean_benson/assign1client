<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>Chained Services demo</title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<h1>Chained services demo</h1>

<%-- loop to print IDs from the session --%>
<c:if test="${not empty sessionScope.eventid}">
	Event IDs for the session:<br>
	<c:forEach var="ID" items="${sessionScope.eventid}">
		<c:out value="${ID}"/><br>
	</c:forEach>
</c:if>

<select onChange="dropdownTip()" id="select" name="search_type" style="margin-right:10px; margin-top:2px;">
    <option selected="selected" value="importDownloadConvert">Import->Download->Convert</option>    
    <option value="importSummary">Import->Summary</option>
    <option value="convertSummary">Convert->Summary</option>
</select>


<script type="text/javascript">
	function dropdownTip(){
		var value = document.getElementById('select').value;
		if(value == "importDownloadConvert"){
			document.getElementById('icd').style.display= 'block';
			document.getElementById('is').style.display= 'none';
			document.getElementById('cs').style.display= 'none';
		}
		if(value == "importSummary"){
			document.getElementById('is').style.display= 'block';
			document.getElementById('cs').style.display= 'none';
			document.getElementById('icd').style.display= 'none';
		}
		if(value == "convertSummary"){
			document.getElementById('cs').style.display= 'block';
			document.getElementById('is').style.display= 'none';
			document.getElementById('icd').style.display= 'none';
		}
	}
</script>

<div id="icd">
	<form action="importConvertDownload" method="POST">
		Sec: <input type="text" name="sec">
		<br>
		startDate: <input type="text" name="startDate" /><br>
		endDate: <input type="text" name="endDate" /><br>
		dataSourceURL: <input type="text" name="dataSourceURL" /><br>
		targetCurrency: <input type="text" name="targetCurrency" /><br>
		filetype: <input type="text" name="fileType" /><br>
		<input type="submit" value="Submit" />
	</form>
</div>

<div id="is" style="display:none">
	<form action="importSummary" method="POST">
		Sec: <input type="text" name="sec">
		<br />
		startDate: <input type="text" name="startDate" /><br>
		endDate: <input type="text" name="endDate" /><br>
		dataSourceURL: <input type="text" name="dataSourceURL" /><br>
		<input type="submit" value="Submit" />
	</form>
</div>

<div id="cs" style="display:none">
	<form action="convertSummary" method="POST">
		eventsetID: <input type="text" name="eventSetId" /><br>
		targetCurrency: <input type="text" name="targetCurrency" /><br>
		<input type="submit" value="Submit" />
	</form>
</div>

<%-- see if we have summary data or a download url --%>
<c:if test="${not empty summary }">
	Summary Results:<br>
	Sec: <c:out value="${summary.sec}"/><br>
	Start Date: <c:out value="${summary.startDate }"/><br>
	End Date: <c:out value="${summary.endDate }"/><br>
	Currency: <c:out value="${summary.currencyCode }"/><br>
	EventID: <c:out value="${summary.eventSetId }"/><br>
	Number of Lines: <c:out value="${summary.numberOfLines }"/><br>
	
	<c:if test="${not empty error}">
		Error: ${error}
	</c:if>
</c:if>

<c:if test="${not empty returnData }">
	Link to download file: <a href="${returnData}">${returnData}</a>
</c:if>

</body>
</html>