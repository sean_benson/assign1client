package au.edu.unsw.soacourse.assign1client;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.endpoint.ClientImpl.EchoContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.edu.unsw.sltf.services.DownloadFile;
import au.edu.unsw.sltf.services.ImportDownloadFaultException;
import au.edu.unsw.sltf.services.ImportDownloadServicesPortType;
import au.edu.unsw.sltf.services.ImportMarketData;
import au.edu.unsw.sltf.services.ImportMarketDataResponse;
import au.edu.unsw.soacourse.service1.ImportDownloadServicesPortTypeImplService;
import au.edu.unsw.soacourse.service2.*;
import au.edu.unsw.soacourse.service2.ExchangeRateFaultMsg;
import au.edu.unsw.soacourse.service3.*;
import au.edu.unsw.soacourse.service3.SummaryMarketData.Arg0;
import au.edu.unsw.soacourse.service3.SummaryMarketDataResponse.Return;


@Controller
public class MarketServiceController {

   // TODO: Add the TopDownSimpleService interface as a member of the controller
   @Autowired
   ImportDownloadServicesPortType service1;
   @Autowired
   SOAPCurrencyConvertService service2;
   @Autowired
   MarketDataUtilService service3;
   @RequestMapping(value = "/",  method = RequestMethod.GET)
   public String home(Model model){
	   model.addAttribute("import", new au.edu.unsw.sltf.services.ObjectFactory().createImportMarketData());
	   model.addAttribute("down", new au.edu.unsw.sltf.services.ObjectFactory().createDownloadFile());
	   
	   
	   
	   return "home";
   }
   
   @RequestMapping(value = "/tryService1",  method = RequestMethod.GET)
   public String tryService1(Model model){
	   model.addAttribute("import", new au.edu.unsw.sltf.services.ObjectFactory().createImportMarketData());
	   model.addAttribute("down", new au.edu.unsw.sltf.services.ObjectFactory().createDownloadFile());
	   
	   
	   return "tryService1";
   }
   
   @RequestMapping(value = "/tryService2",  method = RequestMethod.GET)
   public String tryService2(Model model){
	   model.addAttribute("conv", new au.edu.unsw.soacourse.service2.ExchangeRateRequestMsg());
	   
	   
	   return "tryService2";
   }
   
   @RequestMapping(value = "/tryService3",  method = RequestMethod.GET)
   public String tryService3(Model model){
	   model.addAttribute("convMarketData", new au.edu.unsw.soacourse.service3.ObjectFactory().createConvertMarketDataRequestMsg());
	   model.addAttribute("summMarketData", new au.edu.unsw.soacourse.service3.ObjectFactory().createSummaryMarketDataResponseMsg());
	   
	   
	   return "tryService3";
   }
   
   @RequestMapping("/importMarket")
   public String processImportMarketData(@ModelAttribute("import") ImportMarketData model, Model m) throws Exception {
      // TODO: Add the creation of an impoartMarketData request type and populate it
	 
	  String sec = (String) model.getSec();
	  String startDate = (String) model.getStartDate();
	  String endDate = (String) model.getEndDate();
	  String dataSourceURL = (String) model.getDataSourceURL();
	  /*String sec = "ABCD";
	  String startDate = "10-10-11";
	  String endDate = "10-10-10";
	  String dataSourceURL = "https://www.cse.unsw.edu.au/~cs9322/ass/common/files_csv_spec/FinDataSimple.csv";*/
	  
      
	  String resp="";
	  try{
	  resp = service1.importMarketData(sec, startDate, endDate, dataSourceURL);
	  }
	  catch(ImportDownloadFaultException e){
		resp = e.getMessage();  
	  }
      // TODO: Replace null with the results from the web service response.
      m.addAttribute("returnData", resp);
      
      // View we are returning to, in this case processImportMarketData.jsp 
      return "processImportMarketData";
   }

   @RequestMapping("/downloadFile")
   public String processDownloadFile(ModelMap model, @ModelAttribute("download") DownloadFile down ) throws Exception {
      // TODO: Add the creation of a DownloadFile request type and populate it
      String eventSetId = down.getEventSetId();
      String fileType = down.getFileType();
      // TODO: Call the web service      
	   String resp="";
		  try{
		  resp = service1.downloadFile(eventSetId, fileType);
		  }
		  catch(ImportDownloadFaultException e){
			resp = e.getMessage();  
		  }
	      // TODO: Replace null with the results from the web service response.
	      model.addAttribute("returnData", resp);
      
      
      // View we are returning to, in this case processImportMarketData.jsp 
      return "processDownloadFile";
   }
   
   @RequestMapping("/currencyConvert")
   String currencyConvert(ModelMap model, @ModelAttribute("conv") au.edu.unsw.soacourse.service2.ExchangeRateRequestMsg ex){
	   
	   ExchangeRateResponseMsg resp = new au.edu.unsw.soacourse.service2.ExchangeRateResponseMsg();
	   try{
			  resp = service2.yahooExchangeRate(ex);
			  }
			  catch(ExchangeRateFaultMsg e){
				model.addAttribute("error", e.getMessage());  
			  }
		      // TODO: Replace null with the results from the web service response.
		      model.addAttribute("returnData", resp);
	   
	   return "processCurrencyConvert";
   }
   @RequestMapping("/convertMarketData")
   String convertMarketData(ModelMap model, @ModelAttribute("convMarketData") ConvertMarketDataRequestMsg cnv){
	   ConvertMarketDataResponseMsg resp = new ConvertMarketDataResponseMsg();
	   try{
			  resp = service3.convertMarketData(cnv);
			  }
			  catch (au.edu.unsw.soacourse.service3.ExchangeRateFaultMsg e) {
				// TODO Auto-generated catch block
				  model.addAttribute("error", e.getMessage());
			}
		      // TODO: Replace null with the results from the web service response.
		      model.addAttribute("returnData", resp);
	   
	   //to do tester
	   
	   return "processConvertMarketData";
   }
   @RequestMapping("/summaryMarketData")
   String summaryMarketData(ModelMap model, @ModelAttribute("summMarketData") Arg0 summ){
	   Return resp = new Return();
	   try{
			  resp = service3.summaryMarketData(summ);
			  }
			  catch (au.edu.unsw.soacourse.service3.ExchangeRateFaultMsg e) {
				// TODO Auto-generated catch block
				  model.addAttribute("error", e.getMessage());
			}
		      // TODO: Replace null with the results from the web service response.
		      model.addAttribute("returnData", resp);
	 //to do tester
	   return "processSummaryMarketData";
   }
   @RequestMapping("/chainedServices")
   String chainedServices(){
	   return "chainedServices";
   }
   
   @RequestMapping("/importConvertDownload")
   String importConvertDownload(HttpServletRequest req, ModelMap model){
	   //import market data
	   String sec = (String) req.getParameter("sec");
		  String startDate = (String) req.getParameter("startDate");
		  String endDate = (String) req.getParameter("endDate");
		  String dataSourceURL= (String) req.getParameter("dataSourceURL");
	   String resp="";
		  try{
		  resp = service1.importMarketData(sec, startDate, endDate, dataSourceURL);
		  }
		  catch(ImportDownloadFaultException e){
			resp = e.getMessage();  
		  }
		  //convert, the target currency has been passed from the jsp, but need to set eventID
		  ConvertMarketDataResponseMsg cresp = new ConvertMarketDataResponseMsg();
		  cresp.setEventSetId(resp);
		  ConvertMarketDataRequestMsg c = new ConvertMarketDataRequestMsg();
		  c.setEventSetId(resp);
		  c.setTargetCurrency((String) req.getParameter("targetCurrency"));
		   try{
				  cresp = service3.convertMarketData(c);
				  }
				  catch (au.edu.unsw.soacourse.service3.ExchangeRateFaultMsg e) {
					// TODO Auto-generated catch block
					  model.addAttribute("error", e.getMessage());
				}
		   //download the file
		   String link="";
			  try{
			  link = service1.downloadFile(resp, (String) req.getParameter("fileType"));
			  }
			  catch(ImportDownloadFaultException e){
				resp = e.getMessage();  
			  }
		      // TODO: Replace null with the results from the web service response.
		      //add the result eventid to the session
			  addIDSession(req, resp);
			  model.addAttribute("returnData", link);
	   
	   return "chainedServices";
   }
   
   @RequestMapping("/convertSummary")
   String convertSummary(HttpServletRequest req, ModelMap model){
	   ConvertMarketDataRequestMsg c = new ConvertMarketDataRequestMsg();
	   c.setEventSetId(req.getParameter("eventSetId"));
	   c.setTargetCurrency(req.getParameter("targetCurrency"));
	   ConvertMarketDataResponseMsg cresp = new ConvertMarketDataResponseMsg();
	   try{
			  cresp = service3.convertMarketData(c);
			  }
			  catch (au.edu.unsw.soacourse.service3.ExchangeRateFaultMsg e) {
				// TODO Auto-generated catch block
				  model.addAttribute("error", e.getMessage());
			}
	   //summarise the repsonse
	   Arg0 summReq = new SummaryMarketData.Arg0();
	   summReq.setEventSetId(cresp.getEventSetId());
	   Return summResp = new SummaryMarketDataResponse.Return();
	   try{
		   summResp = service3.summaryMarketData(summReq);
	   }
	   catch(au.edu.unsw.soacourse.service3.ExchangeRateFaultMsg e){
		   model.addAttribute("error", e.getMessage());
	   }
	   //add the response to the model for display
	   model.addAttribute("summary", summResp);
	   addIDSession(req,cresp.getEventSetId() );
	   return "chainedServices";
   }
   
   @RequestMapping("/importSummary")
   String importSummary(HttpServletRequest req, ModelMap model){
	 //import market data
	   String sec = (String) req.getParameter("sec");
		  String startDate = (String) req.getParameter("startDate");
		  String endDate = (String) req.getParameter("endDate");
		  String dataSourceURL= (String) req.getParameter("dataSourceURL");
	   String resp="";
		  try{
		  resp = service1.importMarketData(sec, startDate, endDate, dataSourceURL);
		  }
		  catch(ImportDownloadFaultException e){
			resp = e.getMessage();  
		  }
		//summarise the repsonse
		   Arg0 summReq = new SummaryMarketData.Arg0();
		   summReq.setEventSetId(resp);
		   Return summResp = new SummaryMarketDataResponse.Return();
		   try{
			   summResp = service3.summaryMarketData(summReq);
		   }
		   catch(au.edu.unsw.soacourse.service3.ExchangeRateFaultMsg e){
			   model.addAttribute("error", e.getMessage());
		   }
		   //add the response to the model for display
		   model.addAttribute("summary", summResp);
		   addIDSession(req, resp);
	   return "chainedServices";
   }
   
   private void addIDSession(HttpServletRequest req, String ID){
	   //test if object present
	   if(null == req.getSession().getAttribute("eventid")){
		   ArrayList<String> eventid = new ArrayList<String>();
		   req.getSession().setAttribute("eventid", eventid);
		   }
	   ArrayList eventids =	(ArrayList) req.getSession().getAttribute("eventid");
	   eventids.add(ID);
   }
   
   

   

}