<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>Service 3 demo</title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<h1>Service 3 - Convert Market Data and Summary Market Data</h1>
<b>Convert Market Data demo</b>
<form:form modelAttribute="convMarketData" action="convertMarketData" method="post">
<table>  
    <tbody><tr>
    	<td><form:label path="eventSetId">eventSetId</form:label></td>
    	<td><form:input path="eventSetId"></form:input></td>
    	</tr><tr>
    	<td><form:label path="targetCurrency">Target currency</form:label></td>
    	<td><form:input path="targetCurrency"></form:input></td>
    	</tr>
    	
     </tbody></table> 
     <input name="" type="submit" value="Get Download URL">    
     <input name="" type="reset" value="Reset">
</form:form>
<br><br>
<b>Summary Market Data demo</b>
<form:form modelAttribute="summMarketData" action="summaryMarketData" method="post">
<table>  
    <tbody><tr>
    	<td><form:label path="eventSetId">eventSetId</form:label></td>
    	<td><form:input path="eventSetId"></form:input></td>
    	</tr>
    	
     </tbody></table> 
     <input name="" type="submit" value="Get Download URL">    
     <input name="" type="reset" value="Reset">   
</form:form>

<br><br><br>
<br><b>Example ConvertMarketData:</b>
<br>Enter in the following details:
<br>eventSetId: 260101290201-789904484
<br>targetCurrency: JPY
<br>
<br><b>Example Summary Market Data:</b>
<br>eventSetId: 260101290201-789904484JPY

</body>
</html>