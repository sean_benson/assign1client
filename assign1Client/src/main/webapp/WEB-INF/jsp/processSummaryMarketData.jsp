<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>SummaryMarketData</title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<c:choose>
<c:when test="${ empty error }">
Currency Code: <c:out value = "${ returnData.currencyCode }"></c:out><br>
endDate: <c:out value = "${ returnData.endDate }"></c:out><br>
startDate: <c:out value = "${ returnData.startDate }"></c:out><br>
eventSetId: <c:out value = "${ returnData.eventSetId }"></c:out><br>
numberOfLines: <c:out value = "${ returnData.numberOfLines }"></c:out><br>
sec: <c:out value = "${ returnData.sec }"></c:out><br>
</c:when>
<c:otherwise>
<c:out value="${ error }"></c:out>
</c:otherwise>
</c:choose>
</body>
</html>