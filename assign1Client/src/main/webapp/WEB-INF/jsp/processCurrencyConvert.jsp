<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>CurrencyConvert </title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<c:choose>
<c:when test="${not empty error}">
Error: ${error}
</c:when>
<c:otherwise>
Exchange rate: <b><c:out value="${returnData.rate}"></c:out></b>
As at<c:out value="${returnData.asAt}"></c:out>
</c:otherwise>
</c:choose>
</body>
</html>