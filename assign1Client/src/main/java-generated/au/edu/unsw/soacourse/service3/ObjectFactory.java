
package au.edu.unsw.soacourse.service3;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the au.edu.unsw.soacourse.service3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConvertMarketData_QNAME = new QName("http://service3.soacourse.unsw.edu.au/", "ConvertMarketData");
    private final static QName _ConvertMarketDataResponse_QNAME = new QName("http://service3.soacourse.unsw.edu.au/", "ConvertMarketDataResponse");
    private final static QName _SummaryMarketData_QNAME = new QName("http://service3.soacourse.unsw.edu.au/", "SummaryMarketData");
    private final static QName _SummaryMarketDataResponse_QNAME = new QName("http://service3.soacourse.unsw.edu.au/", "SummaryMarketDataResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: au.edu.unsw.soacourse.service3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SummaryMarketDataResponse }
     * 
     */
    public SummaryMarketDataResponse createSummaryMarketDataResponse() {
        return new SummaryMarketDataResponse();
    }

    /**
     * Create an instance of {@link SummaryMarketData }
     * 
     */
    public SummaryMarketData createSummaryMarketData() {
        return new SummaryMarketData();
    }

    /**
     * Create an instance of {@link ConvertMarketData }
     * 
     */
    public ConvertMarketData createConvertMarketData() {
        return new ConvertMarketData();
    }

    /**
     * Create an instance of {@link ConvertMarketDataResponse }
     * 
     */
    public ConvertMarketDataResponse createConvertMarketDataResponse() {
        return new ConvertMarketDataResponse();
    }

    /**
     * Create an instance of {@link SummaryMarketDataRequest }
     * 
     */
    public SummaryMarketDataRequest createSummaryMarketDataRequest() {
        return new SummaryMarketDataRequest();
    }

    /**
     * Create an instance of {@link SummaryMarketDataResponseMsg }
     * 
     */
    public SummaryMarketDataResponseMsg createSummaryMarketDataResponseMsg() {
        return new SummaryMarketDataResponseMsg();
    }

    /**
     * Create an instance of {@link ConvertMarketDataRequestMsg }
     * 
     */
    public ConvertMarketDataRequestMsg createConvertMarketDataRequestMsg() {
        return new ConvertMarketDataRequestMsg();
    }

    /**
     * Create an instance of {@link ConvertMarketDataResponseMsg }
     * 
     */
    public ConvertMarketDataResponseMsg createConvertMarketDataResponseMsg() {
        return new ConvertMarketDataResponseMsg();
    }

    /**
     * Create an instance of {@link ServiceFaultType }
     * 
     */
    public ServiceFaultType createServiceFaultType() {
        return new ServiceFaultType();
    }

    /**
     * Create an instance of {@link SummaryMarketDataResponse.Return }
     * 
     */
    public SummaryMarketDataResponse.Return createSummaryMarketDataResponseReturn() {
        return new SummaryMarketDataResponse.Return();
    }

    /**
     * Create an instance of {@link SummaryMarketData.Arg0 }
     * 
     */
    public SummaryMarketData.Arg0 createSummaryMarketDataArg0() {
        return new SummaryMarketData.Arg0();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertMarketData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service3.soacourse.unsw.edu.au/", name = "ConvertMarketData")
    public JAXBElement<ConvertMarketData> createConvertMarketData(ConvertMarketData value) {
        return new JAXBElement<ConvertMarketData>(_ConvertMarketData_QNAME, ConvertMarketData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConvertMarketDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service3.soacourse.unsw.edu.au/", name = "ConvertMarketDataResponse")
    public JAXBElement<ConvertMarketDataResponse> createConvertMarketDataResponse(ConvertMarketDataResponse value) {
        return new JAXBElement<ConvertMarketDataResponse>(_ConvertMarketDataResponse_QNAME, ConvertMarketDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SummaryMarketData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service3.soacourse.unsw.edu.au/", name = "SummaryMarketData")
    public JAXBElement<SummaryMarketData> createSummaryMarketData(SummaryMarketData value) {
        return new JAXBElement<SummaryMarketData>(_SummaryMarketData_QNAME, SummaryMarketData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SummaryMarketDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service3.soacourse.unsw.edu.au/", name = "SummaryMarketDataResponse")
    public JAXBElement<SummaryMarketDataResponse> createSummaryMarketDataResponse(SummaryMarketDataResponse value) {
        return new JAXBElement<SummaryMarketDataResponse>(_SummaryMarketDataResponse_QNAME, SummaryMarketDataResponse.class, null, value);
    }

}
