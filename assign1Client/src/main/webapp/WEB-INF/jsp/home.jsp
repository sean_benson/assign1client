<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>Assign 1 WebServices</title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<h1>Welcome to Sean and Phillip's web service!</h1>

<p>We have 3 lovely services for you to enjoy:</p>

<p><b>Service1:</b>
	<br>
	Allows you to filter market data based on date and security codes and then download the result in csv, html or xml
	<br>
	<a href="http://localhost:8080/assign1Client/tryService1">Try the demo</a>
	<a href="http://localhost:8080/assign1/ImportDownloadServices?wsdl">Wizzdol</a>
</p>

<p><b>Service2:</b>
	<br>
	Our very own proprietary currency converter. Convert from one currency to another by supplying the codes
	<br>
	<a href="http://localhost:8080/assign1Client/tryService2">Try the demo</a>
	<a href="http://localhost:8080/assign1/SOAPCurrencyConvertService?wsdl">Wizzdol</a>
</p>

<p><b>Service3:</b>
	<br>
	Convert a data set from service 1 into another currency and also summarise the data you have filtered.
	<br>
	<a href="http://localhost:8080/assign1Client/tryService3">Try the demo</a>
	<a href="http://localhost:8080/assign1/MarketDataUtilService?wsdl">Wizzdol</a>
</p>

<p><b>Chained Services:</b>
	<br>
	Convert a data set from service 1 into another currency and also summarise the data you have filtered.
	<br>
	<a href="http://localhost:8080/assign1Client/chainedServices">Try the demo</a>
	<a href="http://localhost:8080/assign1/MarketDataUtilService?wsdl">Wizzdol</a>
</p>



    

</body>
</html>