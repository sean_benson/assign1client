<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>Service 1 demo</title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<h1>Service 1 - Import Market data and Download File</h1>
<form:form modelAttribute="import" action="importMarket" method="post">
<b>Import market data function demo</b>
<table>  
    <tbody><tr>
    	<td><form:label path="sec">Sec</form:label></td>
    	<td><form:input path="sec"></form:input></td>
    	</tr><tr>
    	<td><form:label path="startDate">startDate</form:label></td>
    	<td><form:input path="startDate"></form:input></td>
    	</tr><tr>
    	<td><form:label path="endDate">endDate</form:label></td>
    	<td><form:input path="endDate"></form:input></td>
    	</tr><tr>
    	<td><form:label path="dataSourceURL">dataSourceURL</form:label></td>
    	<td><form:input path="dataSourceURL"></form:input></td>
    	</tr>
     </tbody></table>    
     <input name="" type="submit" value="Save">    
     <input name="" type="reset" value="Reset">
</form:form >
<br><br>
<b>Download file function demo</b>
<br>
<form:form modelAttribute="down" action="downloadFile" method="post">
<table>  
    <tbody><tr>
    	<td><form:label path="eventSetId">eventSetId</form:label></td>
    	<td><form:input path="eventSetId"></form:input></td>
    	</tr><tr>
    	<td><form:label path="fileType">FileType</form:label></td>
    	<td><form:input path="fileType"></form:input></td>
    	</tr>
    	
     </tbody></table> 
     <input name="" type="submit" value="Get Download URL">    
                                        <input name="" type="reset" value="Reset">   
</form:form>

<br><br><br>
<br><b>Example importMarktData:</b>
<br>Enter in the following details:
<br>sec: ABCD
<br>startDate: 26-01-2001
<br>endDate: 29-02-2001
<br>url: https://www.cse.unsw.edu.au/~cs9322/ass/common/files_csv_spec/FinDataSimple.csv
<br>
<br><b>Example downloadFile:</b>
<br>Enter in the following details:
<br>eventSetId: 260101290201-789904484
<br>FileType(xml/html/csv): xml
<br>

</body>
</html>