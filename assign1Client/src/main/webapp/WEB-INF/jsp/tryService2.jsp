<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>Service 2 demo</title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<form:form modelAttribute="conv" action="currencyConvert" method="post">
<h1>Service 2 - Yahoo Exchange Rate</h1>
<b>Yahoo currency conversion function demo</b>
<table>  
    <tbody><tr>
    	<td><form:label path="baseCurrencyCode">Base currency</form:label></td>
    	<td><form:input path="baseCurrencyCode"></form:input></td>
    	</tr><tr>
    	<td><form:label path="targetCurrencyCode">Target currency</form:label></td>
    	<td><form:input path="targetCurrencyCode"></form:input></td>
    	</tr>
    	<tr>
     </tbody></table> 
     <input name="" type="submit" value="Convert">
     <input name="" type="reset" value="Reset">
</form:form>

<br>
<br><b>Example Yahoo currency conversion:</b>
<br>Base Currency: AUD
<br>Target Currency: USD
<br>

</body>
</html>